
public class Main {

	public static void main(String[] args) {
		
		Member member = new Member("John Doe");
		Library library = new Library();
		
		library.addBook(new Publication("Book", "authorA", true));
		library.addBook(new Publication("Textbook", "authorB", true));
		library.addBook(new Publication("Magazine", "authorC", false));
		library.addBook(new Publication("Newspaper", "authorD", false));
		
		library.borrowBook(member, "Book");
		library.borrowBook(member, "Textbook");
		
		//this 2 won't add into member record
		library.borrowBook(member, "Magazine");
		library.borrowBook(member, "Newspaper");
		
		System.out.println("Member: " + member.getName()+ " books");
		for (Publication pub:member.getRecord()) {
			System.out.println("-" +pub.getTitle());
		}
		
		library.returnBook(member, "Book");
		
		System.out.println("Member: " + member.getName()+ " books (After return)");
		for (Publication pub:member.getRecord()) {
			System.out.println("-" +pub.getTitle());
		}
		
	}

}
